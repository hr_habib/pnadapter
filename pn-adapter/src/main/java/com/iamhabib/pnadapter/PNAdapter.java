package com.iamhabib.pnadapter;

import android.content.Context;
import android.support.annotation.LayoutRes;
import android.view.LayoutInflater;
import android.view.View;

import java.util.ArrayList;

/**
 * Created by HABIB on 10/31/2016.
 */

public class PNAdapter {

    private static final int POSITION_LEFT=0;
    private static final int POSITION_MIDDLE=1;
    private static final int POSITION_RIGHT=2;
    Context context;
    ArrayList<String> dataList=new ArrayList<>();
    ArrayList<View> viewList=new ArrayList<>(3);
    private int currestViewPosition=0;
    private @LayoutRes int layoutView;
    View view;

    public PNAdapter(Context context, @LayoutRes int layoutView, ArrayList<String> list) {
        this.context=context;
        this.layoutView = layoutView;
        this.dataList = list;
        currestViewPosition=0;
        viewList.add(POSITION_LEFT, generateView(0));
        viewList.add(POSITION_MIDDLE, generateView(1));
        viewList.add(POSITION_RIGHT, generateView(2));
    }

    public View getInitView(){
        return viewList.get(POSITION_LEFT);
    }

    public View getPreviousView(){
        if(currestViewPosition==0){
            return null;
        }else if(currestViewPosition==1){
            currestViewPosition=0;
            return viewList.get(POSITION_LEFT);
        }else if(currestViewPosition==dataList.size()-1){
            currestViewPosition=currestViewPosition-1;
            return viewList.get(POSITION_MIDDLE);
        }else if(currestViewPosition>1){
            currestViewPosition=currestViewPosition-1;
            view=viewList.get(POSITION_LEFT);
            viewList.remove(POSITION_RIGHT);
            viewList.add(POSITION_RIGHT, viewList.get(POSITION_MIDDLE));
            viewList.remove(POSITION_MIDDLE);
            viewList.add(POSITION_MIDDLE,view);
            viewList.add(POSITION_LEFT,generateView(currestViewPosition-1));
            return view;
        }
        return null;
    }

    public View getNextView(){
        if(currestViewPosition==0){
            currestViewPosition=currestViewPosition+1;
            return viewList.get(POSITION_MIDDLE);
        }else if(currestViewPosition==dataList.size()-1){
            return null;
        }else if(currestViewPosition==dataList.size()-2){
            currestViewPosition=currestViewPosition+1;
            return viewList.get(POSITION_RIGHT);
        }else{
            currestViewPosition=currestViewPosition+1;
            view=viewList.get(POSITION_RIGHT);
            viewList.remove(POSITION_LEFT);
            viewList.add(POSITION_LEFT, viewList.get(POSITION_MIDDLE));
            viewList.remove(POSITION_MIDDLE);
            viewList.add(POSITION_MIDDLE,view);
            viewList.add(POSITION_RIGHT,generateView(currestViewPosition+1));
            return view;
        }
    }

    public View getView(int index){
        if(index<dataList.size() && index>-1){
            if(index==0){
                currestViewPosition=index;
                viewList.add(POSITION_LEFT, generateView(0));
                viewList.add(POSITION_MIDDLE, generateView(1));
                viewList.add(POSITION_RIGHT, generateView(2));
                return viewList.get(POSITION_LEFT);
            }else if(index==1){
                currestViewPosition=index;
                viewList.add(POSITION_LEFT, generateView(0));
                viewList.add(POSITION_MIDDLE, generateView(1));
                viewList.add(POSITION_RIGHT, generateView(2));
                return viewList.get(POSITION_MIDDLE);
            }else if(index==dataList.size()-1){
                currestViewPosition=index;
                viewList.remove(POSITION_LEFT);
                viewList.remove(POSITION_MIDDLE);
                viewList.remove(POSITION_RIGHT);
                view=generateView(index+1);
                viewList.add(POSITION_LEFT, generateView(index-2));
                viewList.add(POSITION_MIDDLE, generateView(index-1));
                viewList.add(POSITION_RIGHT, view);
                return view;
            }else {
                currestViewPosition=index;
                viewList.remove(POSITION_LEFT);
                viewList.remove(POSITION_MIDDLE);
                viewList.remove(POSITION_RIGHT);
                view=generateView(index);
                viewList.add(POSITION_LEFT, generateView(index-1));
                viewList.add(POSITION_MIDDLE, view);
                viewList.add(POSITION_RIGHT, generateView(index+1));
                return view;
            }
        }
        return null;
    }

    private View generateView(int index){
        View v= LayoutInflater.from(context).inflate(layoutView, null);

        return v;
    }
}
